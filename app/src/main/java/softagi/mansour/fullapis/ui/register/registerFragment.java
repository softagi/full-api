package softagi.mansour.fullapis.ui.register;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import softagi.mansour.fullapis.R;
import softagi.mansour.fullapis.models.*;
import softagi.mansour.fullapis.network.remote.retrofitClient;

public class registerFragment extends Fragment
{
    private View mainView;
    private EditText nameField;
    private EditText emailField;
    private EditText passwordField;
    private EditText passwordConfirmationField;
    private EditText mobileField;
    private Spinner countrySpinner;
    private Spinner citySpinner;
    private Spinner genderSpinner;
    private CardView countryCard;
    private CardView cityCard;
    private Button registerBtn;
    private ProgressDialog progressDialog;

    private String gender;
    private String country;
    private String city;

    private List<String> genderData = new ArrayList<>();
    private List<String> countryData = new ArrayList<>();
    private List<String> cityData = new ArrayList<>();
    private List<countryModel.countryData> countryDataFromServer = new ArrayList<>();
    private List<cityModel.cityData> cityDataFromServer = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_register, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initViews();
        initDialog();
        setGenderSpinner();
        getCountryData();
    }

    private void getCountryData()
    {
        progressDialog.show();
        Call<countryModel> call = retrofitClient.getInstance().getCountries();
        call.enqueue(new Callback<countryModel>() {
            @Override
            public void onResponse(Call<countryModel> call, Response<countryModel> response)
            {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    countryData.add("select country");
                    countryDataFromServer = response.body().getData();
                    for (countryModel.countryData b : countryDataFromServer)
                    {
                        countryData.add(b.getCountry_name());
                    }
                    setCountrySpinner(countryData,countryDataFromServer);
                }
            }

            @Override
            public void onFailure(Call<countryModel> call, Throwable t)
            {

            }
        });
    }

    private void setCountrySpinner(final List<String> countryData, final List<countryModel.countryData> countryDataFromServer)
    {
        countryCard.setVisibility(View.VISIBLE);
        ArrayAdapter countrySpinnerArrayAdapter = new ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, countryData);
        countrySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(countrySpinnerArrayAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position != 0)
                {
                    country = countryDataFromServer.get(position - 1).getId();
                    cityData.clear();
                    getCityData(country);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getCityData(String id)
    {
        progressDialog.show();
        Call<cityModel> call = retrofitClient.getInstance().getCities(id);
        call.enqueue(new Callback<cityModel>() {
            @Override
            public void onResponse(Call<cityModel> call, Response<cityModel> response)
            {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    cityData.add("select city");
                    cityDataFromServer = response.body().getData();
                    for (cityModel.cityData b : cityDataFromServer)
                    {
                        cityData.add(b.getCountry_name());
                    }
                    setCitySpinner(cityData,cityDataFromServer);
                }
            }

            @Override
            public void onFailure(Call<cityModel> call, Throwable t)
            {

            }
        });
    }

    private void setCitySpinner(final List<String> cityData, final List<cityModel.cityData> cityDataFromServer)
    {
        cityCard.setVisibility(View.VISIBLE);

        ArrayAdapter citySpinnerArrayAdapter = new ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, cityData);
        citySpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        citySpinner.setAdapter(citySpinnerArrayAdapter);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position != 0)
                {
                    city = cityDataFromServer.get(position - 1).getId();
                    Toast.makeText(getContext(), city, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setGenderSpinner()
    {
        genderData.add("gender");
        genderData.add("male");
        genderData.add("female");

        ArrayAdapter genderSpinnerArrayAdapter = new ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, genderData);
        genderSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderSpinnerArrayAdapter);

        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if (position != 0)
                {
                    gender = genderData.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initDialog()
    {
        progressDialog = new ProgressDialog(requireContext());
        progressDialog.setMessage("loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
    }

    private void initViews()
    {
        nameField = mainView.findViewById(R.id.name_field);
        emailField = mainView.findViewById(R.id.email_field);
        passwordField = mainView.findViewById(R.id.password_field);
        passwordConfirmationField = mainView.findViewById(R.id.password_confirmation_field);
        mobileField = mainView.findViewById(R.id.mobile_field);
        countrySpinner = mainView.findViewById(R.id.country_spinner);
        citySpinner = mainView.findViewById(R.id.city_spinner);
        genderSpinner = mainView.findViewById(R.id.gender_spinner);
        countryCard = mainView.findViewById(R.id.country_card);
        cityCard = mainView.findViewById(R.id.city_card);
        registerBtn = mainView.findViewById(R.id.register_btn);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameField.getText().toString();
                String email = emailField.getText().toString();
                String password = passwordField.getText().toString();
                String passwordConfirmation = passwordConfirmationField.getText().toString();
                String mobile = mobileField.getText().toString();

                if (email.isEmpty() || name.isEmpty() || mobile.isEmpty())
                {
                    Toast.makeText(getContext(), "invalid data", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6)
                {
                    Toast.makeText(getContext(), "password is too short", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!passwordConfirmation.equals(password))
                {
                    Toast.makeText(getContext(), "password is not matching", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog.show();
                register(name,email,password,passwordConfirmation,mobile);
            }
        });
    }

    private void register(String name, String email, String password, String passwordConfirmation, String mobile)
    {
/*        Call<loginResponseModel> call = retrofitClient.getInstance().userLoginParams(email, password, "application/json");
        call.enqueue(new Callback<loginResponseModel>()
        {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token(), Toast.LENGTH_SHORT).show();
                    } else
                        {
                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                }
            }

            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t)
            {

            }
        });*/

        Call<registerResponseModel> call = retrofitClient.getInstance().userRegisterFormUrl(
                name,
                email,
                password,
                passwordConfirmation,
                mobile,
                "1",
                "1",
                "yes",
                "male",
                "application/x-www-form-urlencoded"
        );
        call.enqueue(new Callback<registerResponseModel>()
        {
            @Override
            public void onResponse(Call<registerResponseModel> call, Response<registerResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token() + "\n" + response.body().getMessage(), Toast.LENGTH_LONG).show();
                    } else
                    {
                        Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<registerResponseModel> call, Throwable t)
            {

            }
        });

/*        Call<loginResponseModel> call = retrofitClient.getInstance().userLoginRaw(new loginRequestModel(email,password), "application/json");
        call.enqueue(new Callback<loginResponseModel>()
        {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token(), Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t)
            {

            }
        });*/

/*        MultipartBody.Part e = MultipartBody.Part.createFormData("email", email);
        MultipartBody.Part p = MultipartBody.Part.createFormData("password", password);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", );

        Call<loginResponseModel> call = retrofitClient.getInstance().userLoginMulti(e,p);
        call.enqueue(new Callback<loginResponseModel>()
        {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token(), Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t)
            {

            }
        });*/
    }
}