package softagi.mansour.fullapis.ui.welcome;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import softagi.mansour.fullapis.R;
import softagi.mansour.fullapis.ui.login.loginFragment;
import softagi.mansour.fullapis.ui.register.registerFragment;

public class welcomeFragment extends Fragment
{
    private View mainView;
    private Button loginBtn;
    private Button registerBtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_welcome, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initViews();
    }

    private void initViews()
    {
        loginBtn = mainView.findViewById(R.id.login_btn);
        registerBtn = mainView.findViewById(R.id.register_btn);

        loginBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                requireActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new loginFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                requireActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new registerFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
