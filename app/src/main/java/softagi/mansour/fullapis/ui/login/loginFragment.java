package softagi.mansour.fullapis.ui.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import softagi.mansour.fullapis.models.*;
import softagi.mansour.fullapis.network.remote.*;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import softagi.mansour.fullapis.R;

public class loginFragment extends Fragment
{
    private View mainView;
    private EditText emailField;
    private EditText passwordField;
    private Button loginBtn;
    private ProgressDialog progressDialog;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mainView = inflater.inflate(R.layout.fragment_login, null);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        initViews();
        initDialog();
        initPref();
    }

    private void initPref()
    {
        sharedPreferences = requireActivity().getSharedPreferences("FULL_PREFERENCES", Context.MODE_PRIVATE);
    }

    private void initDialog()
    {
        progressDialog = new ProgressDialog(requireContext());
        progressDialog.setMessage("loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
    }

    private void initViews()
    {
        emailField = mainView.findViewById(R.id.email_field);
        passwordField = mainView.findViewById(R.id.password_field);
        loginBtn = mainView.findViewById(R.id.login_btn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailField.getText().toString();
                String password = passwordField.getText().toString();

                if (email.isEmpty())
                {
                    Toast.makeText(getContext(), "invalid data", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6)
                {
                    Toast.makeText(getContext(), "password is too short", Toast.LENGTH_SHORT).show();
                    return;
                }

                String token = sharedPreferences.getString("TOKEN", null);
                if (token != null)
                {
                    Toast.makeText(getContext(), token, Toast.LENGTH_SHORT).show();
                    return;
                }

                progressDialog.show();
                login(email, password);
            }
        });
    }

    private void login(String email, String password)
    {
        Call<loginResponseModel> call = retrofitClient.getInstance().userLoginParams(email, password, "application/json");
        call.enqueue(new Callback<loginResponseModel>()
        {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token(), Toast.LENGTH_SHORT).show();
                        editor = sharedPreferences.edit();
                        editor.putString("TOKEN", response.body().getData().getApi_token());
                        editor.apply();
                    } else
                        {
                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                }
            }

            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t)
            {

            }
        });

/*        Call<loginResponseModel> call = retrofitClient.getInstance().userLoginFormUrl(email, password, "application/x-www-form-urlencoded");
        call.enqueue(new Callback<loginResponseModel>()
        {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token(), Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t)
            {

            }
        });*/

/*        Call<loginResponseModel> call = retrofitClient.getInstance().userLoginRaw(new loginRequestModel(email,password), "application/json");
        call.enqueue(new Callback<loginResponseModel>()
        {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token(), Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t)
            {

            }
        });*/

/*        MultipartBody.Part e = MultipartBody.Part.createFormData("email", email);
        MultipartBody.Part p = MultipartBody.Part.createFormData("password", password);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", );

        Call<loginResponseModel> call = retrofitClient.getInstance().userLoginMulti(e,p);
        call.enqueue(new Callback<loginResponseModel>()
        {
            @Override
            public void onResponse(Call<loginResponseModel> call, Response<loginResponseModel> response) {
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();

                    if (response.body().isStatus())
                    {
                        Toast.makeText(getContext(), response.body().getData().getApi_token(), Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<loginResponseModel> call, Throwable t)
            {

            }
        });*/
    }
}