package softagi.mansour.fullapis.models;

import java.util.List;

public class cityModel
{
    private boolean status;
    private List<cityData> data;

    public cityModel(boolean status, List<cityData> data) {
        this.status = status;
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<cityData> getData() {
        return data;
    }

    public void setData(List<cityData> data) {
        this.data = data;
    }

    public class cityData
    {
        private String city_name;
        private String id;

        public cityData(String country_name, String id) {
            this.city_name = country_name;
            this.id = id;
        }

        public String getCountry_name() {
            return city_name;
        }

        public void setCountry_name(String country_name) {
            this.city_name = country_name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}