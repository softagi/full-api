package softagi.mansour.fullapis.models;

public class registerResponseModel
{
    private boolean status;
    private registerData data;
    private String message;

    public registerResponseModel(boolean status, registerData data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public registerData getData() {
        return data;
    }

    public void setData(registerData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class registerData
    {
        private String api_token;

        public registerData(String api_token) {
            this.api_token = api_token;
        }

        public String getApi_token() {
            return api_token;
        }

        public void setApi_token(String api_token) {
            this.api_token = api_token;
        }
    }
}