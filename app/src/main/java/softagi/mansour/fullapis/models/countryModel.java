package softagi.mansour.fullapis.models;

import java.util.List;

public class countryModel
{
    private boolean status;
    private List<countryData> data;

    public countryModel(boolean status, List<countryData> data) {
        this.status = status;
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<countryData> getData() {
        return data;
    }

    public void setData(List<countryData> data) {
        this.data = data;
    }

    public class countryData
    {
        private String country_name;
        private String id;

        public countryData(String country_name, String id) {
            this.country_name = country_name;
            this.id = id;
        }

        public String getCountry_name() {
            return country_name;
        }

        public void setCountry_name(String country_name) {
            this.country_name = country_name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}