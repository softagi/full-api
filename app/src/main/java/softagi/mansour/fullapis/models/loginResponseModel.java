package softagi.mansour.fullapis.models;

public class loginResponseModel
{
    private boolean status;
    private loginData data;
    private String message;

    public loginResponseModel(boolean status, loginData data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public loginData getData() {
        return data;
    }

    public void setData(loginData data) {
        this.data = data;
    }

    public class loginData
    {
        private String api_token;

        public loginData(String api_token) {
            this.api_token = api_token;
        }

        public String getApi_token() {
            return api_token;
        }

        public void setApi_token(String api_token) {
            this.api_token = api_token;
        }
    }
}