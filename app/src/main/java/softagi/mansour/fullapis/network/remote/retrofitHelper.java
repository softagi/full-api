package softagi.mansour.fullapis.network.remote;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.Part;
import retrofit2.http.Query;
import softagi.mansour.fullapis.models.*;
import retrofit2.http.POST;

public interface retrofitHelper
{
    @POST("api/login")
    Call<loginResponseModel> loginWithParams(
            @Query("email") String email,
            @Query("password") String password,
            @Header("Content-Type") String type
    );

    @POST("api/login")
    @FormUrlEncoded
    Call<loginResponseModel> loginWithFormUrlEncoded(
            @Field("email") String email,
            @Field("password") String password,
            @Header("Content-Type") String type
    );

    @POST("api/login")
    Call<loginResponseModel> loginWithRaw(
            @Body loginRequestModel loginRequestModel,
            @Header("Content-Type") String type
    );

    @POST("api/login")
    @Multipart
    Call<loginResponseModel> loginWithMultiPart(
            @Part MultipartBody.Part email,
            @Part MultipartBody.Part password
    );

    @POST("api/register")
    @FormUrlEncoded
    Call<registerResponseModel> registerWithFormUrlEncoded(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("mobile") String mobile,
            @Field("country_id") String country_id,
            @Field("city_id") String city_id,
            @Field("agreement") String agreement,
            @Field("gender") String gender,
            @Header("Content-Type") String type
    );

    @GET("api/get/city/country")
    Call<countryModel> getCountries();

    @GET("api/get/city/country")
    Call<cityModel> getCities(
            @Query("country_id") String id
    );
}