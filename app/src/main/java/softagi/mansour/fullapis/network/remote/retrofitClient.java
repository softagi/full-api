package softagi.mansour.fullapis.network.remote;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import softagi.mansour.fullapis.models.*;

public class retrofitClient
{
    private static retrofitClient retrofitClient;
    private static retrofitHelper retrofitHelper;

    private retrofitClient()
    {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl("http://sci.softagi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitHelper = retrofit.create(retrofitHelper.class);
    }

    public static retrofitClient getInstance()
    {
        if (retrofitClient == null)
        {
            retrofitClient = new retrofitClient();
        }

        return retrofitClient;
    }

    public static Call<loginResponseModel> userLoginParams(String email, String password, String type)
    {
        return retrofitHelper.loginWithParams(email, password, type);
    }

    public static Call<loginResponseModel> userLoginFormUrl(String email, String password, String type)
    {
        return retrofitHelper.loginWithFormUrlEncoded(email, password, type);
    }

    public static Call<loginResponseModel> userLoginRaw(loginRequestModel loginRequestModel, String type)
    {
        return retrofitHelper.loginWithRaw(loginRequestModel, type);
    }

    public static Call<loginResponseModel> userLoginMulti(MultipartBody.Part email , MultipartBody.Part password)
    {
        return retrofitHelper.loginWithMultiPart(email, password);
    }

    public static Call<registerResponseModel> userRegisterFormUrl(
            String name,
            String email,
            String password,
            String password_confirmation,
            String mobile,
            String country_id,
            String city_id,
            String agreement,
            String gender,
            String type
    )
    {
        return retrofitHelper.registerWithFormUrlEncoded(name, email, password, password_confirmation, mobile, country_id, city_id, agreement, gender, type);
    }

    public static Call<countryModel> getCountries()
    {
        return retrofitHelper.getCountries();
    }

    public static Call<cityModel> getCities(String id)
    {
        return retrofitHelper.getCities(id);
    }
}